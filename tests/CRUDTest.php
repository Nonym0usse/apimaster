<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CRUDTest extends WebTestCase {

    public function testGet(){
        $client = static::createClient();

        $client->request('GET', '/pizza/all');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}