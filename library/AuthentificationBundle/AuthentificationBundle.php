<?php

namespace Library\AuthentificationBundle;
use App\Entity\Pizza;
use App\Entity\User;
use App\Repository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use App\Controller\PizzaController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as abs;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthentificationBundle extends AbstractController {

    public function loginAuthentification($email, $password){



        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email, 'password' => $password]);
        if($user != NULL){
            $session = new Session();
            $session->start();

// set and get session attributes
            $session->set('name', $email);
            $session->get('name');

            //création du jwt
            $user_data = array(
                'mail' => $email,
                'password' => $password,
                'date' => date('Y-m-d')
            );
            $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

            $payload = json_encode($user_data);

            $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
            $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

            $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true);

            $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

            $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
            //FIN de création de jwt
            return $jwt;
        }else{
            return "Une erreur s'est produite";        }
    }

    public function registerAuthentification($email, $password){
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $product = $this->getDoctrine()->getManager();
        $product->persist($user);
        $product->flush();

        return 'Created';
    }
}